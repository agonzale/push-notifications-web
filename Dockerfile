FROM node:8.11.3

WORKDIR /usr/src/app

COPY src/ .

RUN cd client && \
  npm install && \
  REACT_APP_BASE_URL=https://pacheco.cern.ch:8080/api \
  REACT_APP_OAUTH_CLIENT_ID=pacheco.cern.ch \
  REACT_APP_AUTHORIZATION_URL=https://oauth.web.cern.ch/OAuth/Authorize \
  REACT_APP_OAUTH_REDIRECT_URL=https://pacheco.cern.ch:3000/redirect \
  npm run build

RUN cd server && npm install

CMD if [ "$NODE_ENV" = "development" ] ; then npm start --prefix $PUSH_SERVICE ; else npm run start-prod --prefix $PUSH_SERVICE ; fi
