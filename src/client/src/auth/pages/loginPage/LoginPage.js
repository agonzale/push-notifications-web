import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'

import './LoginPage.css'
import { withStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'
import LoginButton from 'auth/components/LoginButton'

const styles = theme => ({
    progress: {
        margin: theme.spacing.unit * 2
    }
})

export class LoginPage extends Component {
    render () {
        const { classes } = this.props

        if (this.props.isAuthenticated) {
            return <Redirect exact={true} to={'notifications'} />
        }

        if (this.props.loginInProgress) {
            return <CircularProgress className={classes.progress} />
        }

        return (
            <div className={'LoginPage'}>
                <div className={'padded-item LoginPage__Centered'}>
                    <div className="centered-element">
                        <h2 className="ui center aligned header gray-text">
              loginPageHeader
                        </h2>
                        <LoginButton />
                    </div>
                </div>
            </div>
        )
    }
}

export default withStyles(styles)(LoginPage)
