import { RSAA } from 'redux-api-middleware'
import { withAuth } from 'auth/utils/authUtils.js'

// Get notifications
export const GET_NOTIFICATIONS = 'GET_NOTIFICATIONS'
export const GET_NOTIFICATIONS_SUCCESS = 'GET_NOTIFICATIONS_SUCCESS'
export const GET_NOTIFICATIONS_FAILURE = 'GET_NOTIFICATIONS_FAILURE'

export const getNotifications = () => ({
    [RSAA]: {
        endpoint: process.env.REACT_APP_BASE_URL + '/notifications',
        method: 'GET',
        credentials: 'include',
        headers: withAuth({ 'Content-Type': 'application/json' }),
        types: [
            GET_NOTIFICATIONS,
            GET_NOTIFICATIONS_SUCCESS,
            GET_NOTIFICATIONS_FAILURE
        ]
    }
})
