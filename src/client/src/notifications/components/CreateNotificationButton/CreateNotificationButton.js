import React, { Component, Fragment } from 'react'
import NotificationsForm from 'notifications/containers/CreateNotificationForm'
import {
    Dialog,
    DialogTitle,
    Button,
    IconButton,
    DialogContent
} from '@material-ui/core'
import Clear from '@material-ui/icons/Clear'
import AddIcon from '@material-ui/icons/Add'
import './CreateNotificationButton.css'

class CreateNotificationButton extends Component {
    render() {
        let { buttonStyle, open, openDialog, closeDialog } = this.props
        return (
            <Fragment>
                <Dialog
                    style={{ overflow: 'hidden' }}
                    open={open}
                    onClose={closeDialog}
                >
                    <DialogTitle
                        className="new-notification-dialog-title"
                        disableTypography={true}
                    >
                        <h2>New Notification</h2>
                        <IconButton
                            className="new-notification-dialog-close-button"
                            variant="fab"
                            aria-label="Close"
                            onClick={closeDialog}
                        >
                            <Clear className="close-button" />
                        </IconButton>
                    </DialogTitle>

                    <DialogContent className="new-notification-dialog-content">
                        <NotificationsForm onSuccess={closeDialog} />
                    </DialogContent>
                </Dialog>
                <div className={buttonStyle}>
                    <Button
                        variant="fab"
                        color="primary"
                        aria-label="Add"
                        onClick={openDialog}
                    >
                        <AddIcon />
                    </Button>
                </div>
            </Fragment>
        )
    }
}

export default CreateNotificationButton
