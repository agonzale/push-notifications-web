
import { openCreateNotificationsForm, closeCreateNotificationForm } from './../actions/CreateNotificationFromActions'
import { connect } from 'react-redux'
import CreateNotificationButton from './../components/CreateNotificationButton/CreateNotificationButton'
        
const mapStateToProps = (state) => {
    return {
        open: state.notifications.newNotificationForm.open
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        openDialog: () => dispatch(openCreateNotificationsForm()),
        closeDialog: () => dispatch(closeCreateNotificationForm())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateNotificationButton)