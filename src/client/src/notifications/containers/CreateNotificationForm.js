import { connect } from 'react-redux'
import CreateNotificationForm from 'notifications/components/CreateNotificationForm/CreateNotificationForm'
import { createNotification } from '../actions/CreateNotification'

const mapStateToProps = state => {
    return {
        newNotification: state.notifications.newNotification
    }
}

const mapDispatchToProps = dispatch => {
    return {
        createNotification: (notification) => {
            dispatch(createNotification(notification))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CreateNotificationForm)
