import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as getNotificationsActionCreators from 'notifications/actions/GetNotifications'
import NotificationsList from 'notifications/components/NotificationsList/NotificationsList'

const mapStatetoProps = state => {
  return {
    notifications: state.notifications.notificationsList.notifications
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(getNotificationsActionCreators, dispatch)
}

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(NotificationsList)
