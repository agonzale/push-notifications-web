import NotificationsPage from "notifications/pages/NotificationsPage/NotificationsPage";

export const notificationsRoute = {
  id: "notifications",
  path: "/notifications",
  icon: "inbox",
  text: "Notifications",
  component: NotificationsPage
};
