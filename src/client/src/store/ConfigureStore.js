import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import apiMiddleware from 'middleware'
import rootReducer from 'reducers'

export default function configureSore (history) {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  return createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(apiMiddleware, routerMiddleware(history)))
  )
}
