import { createStore } from "redux";
import rootReducer from "reducers";

export default function configureSore(initialState) {
  return createStore(rootReducer, initialState);
}
