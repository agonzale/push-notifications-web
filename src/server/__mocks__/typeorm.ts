import { User } from "../src/models/user"
const users = [{
  id: "1",
  username: "username"
}]

export const EntityManager = {
  save: jest.fn(),
  findOneOrFail: (instanceType: any, id: string) => {
    if (instanceType === User) {
      return users.find(user => user.id == id)[0]
    }
  }
} 