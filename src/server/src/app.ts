import "reflect-metadata"; // this shim is required
import { useExpressServer, Action } from "routing-controllers";
import { createConnection } from "typeorm";
import * as fs from "fs";
import * as https from "https";
import * as http from "http";
import * as express from "express";
import { AuthorizationChecker } from "./middelware/authorizationChecker";
import { ConfigureENVs } from "./config/configuration";
import { SendInboxEmails } from "./mailbox/mailbox-parser";

// Imports the environmet variables form the *.env files in the root folder
ConfigureENVs.config(process.env.NODE_ENV);

//create express server
const app = express();
let server;

if (process.env.NODE_ENV == "production") {
  server = http.createServer(app);
} else {
  const httpsOptions = {
    cert: fs.readFileSync("/usr/src/app/server/certificate.pem"),
    key: fs.readFileSync("/usr/src/app/server/key.pem")
  };
  server = https.createServer(httpsOptions, app);
}

// creates express app, registers all controller routes and returns you express app instance
useExpressServer(app, {
  cors: {
    origin: ["https://pljmac02.dyndns.cern.ch:3000"],
    credentials: true
  },
  routePrefix: "/api",
  controllers: [__dirname + "/controllers/*.ts"], // we specify controllers we want to use
  authorizationChecker: AuthorizationChecker.check
});

// Create db connection and run express application on port 8080
createConnection()
  .then(() => {
    server.listen(8080, () => console.log("Server running on port 8080"));

    // Send emails in the mail box
    setInterval(
      SendInboxEmails,
      parseInt(process.env.LOOK_FOR_NEW_EMAILS_INTERVAL)
    );
  })
  .catch(error => console.log(error));
