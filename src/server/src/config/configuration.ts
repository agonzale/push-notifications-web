export class ConfigureENVs {
  static config(configuration: String = "test") {
    if (configuration === "test")
      require("dotenv").config({ path: "src/config/.env.test" });
    if (configuration === "development") {
      require("dotenv").config({ path: "src/config/.env.dev" });
    }
  }
}
