import {
  Post,
  JsonController,
  BodyParam,
  Authorized
} from "routing-controllers";
import { ServiceFactory } from "../services/services-factory";
import { ServicesService } from "../services/services-service";

@JsonController("/services")
export class NotificationsController {
  servicesService: ServicesService = ServiceFactory.getServicesService();

  @Authorized("USER")
  @Post()
  createService(@BodyParam("name") serviceName: string) {
    return this.servicesService.createService(serviceName)
  }
}