import * as request from "request-promise-native";
import { RequestPromiseOptions } from "request-promise-native";
import { User } from "../user";

export class CernOauth {

  async getUser(code): Promise<any> {
    if (code == "true")
      return Promise.resolve(JSON.stringify(new User({ id: 1, username: "username" })))
    else
      throw new Error("The user does not exist");
  }
}
