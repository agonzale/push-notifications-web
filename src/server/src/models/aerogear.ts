import { Notification } from "./notification";
const agSender = require("unifiedpush-node-sender");

export class AeroGear {
  private settings = {
    url: process.env.AEROGEAR_URL,
    applicationId: process.env.AEROGEAR_APPLICATION_ID,
    masterSecret: process.env.AEROGEAR_MASTER_SECRET
  };
  private client;
  constructor() {
    agSender(this.settings).then(client => this.client = client);

  }

  async sendNotification(notification: Notification, alias: string) {
    let message = new Message(notification);

    return await this.client.sender.send(message, { criteria: { alias: [alias] } })
  }
}

class Message {
  public alert;
  constructor(notification: Notification) {
    this.alert = notification.body;
  }
}
