import { User } from "./user";
import * as jwt from "jsonwebtoken";
import * as randomStringGenerator from "randomstring";
import { AuthUtils } from "../utils/authUtils";
import { Cache } from "./cache";

export class Authentication {
    constructor() { }

    static getTokens(user: User) {
        const randomString = randomStringGenerator.generate();
        const accessToken = this.generateAccessToken(user, randomString);
        const refreshToken = this.generateRefreshToken(user, randomString);

        return { randomString, accessToken, refreshToken };
    }

    static generateAccessToken(user: User, randomString: string) {
        return jwt.sign(
            {
                id: user.id,
                username: user.username,
                fingerPrint: AuthUtils.hash(randomString)
            },
            process.env.JWT_SECRET,
            {
                expiresIn: parseInt(process.env.JWT_EXPIRATION_TIME) || 60
            }
        );
    }

    static generateRefreshToken(user: User, randomString: string) {
        const payload = {
            id: user.id,
            fingerPrint: AuthUtils.hash(randomString),
            iat: Math.trunc(new Date().getTime() / 1000)
        }
        this.addRefreshTokenToWhiteList(payload)
        return jwt.sign(payload,
            process.env.JWT_SECRET
        );
    }

    static async addRefreshTokenToWhiteList(tokenPayload) {
        Cache.save(tokenPayload.id, tokenPayload)
    }

    static removeRefreshTokenFromWhiteList(tokenPayload) {
        return Cache.delete(tokenPayload.id, tokenPayload)
    }

    static async isRefreshtokenInWhiteList(tokenPayload: any): Promise<boolean> {
        let storedTokens: Array<any> = <Array<any>>await Cache.get(tokenPayload.id)
        return storedTokens.some(t => JSON.stringify(t) === JSON.stringify(tokenPayload))
    }
}
