
import * as redis from "redis"

const options: redis.ClientOpts = {
  host: process.env.REDIS_HOST,
  port: parseInt(process.env.REDIS_PORT)
}

export class Cache {

  static cache: redis.RedisClient = redis.createClient(options);

  static get(key: string) {
    return new Promise(resolve => {
      Cache.cache.smembers(key, (error, members: string[]) => {
        resolve(members.map(m => {
          return JSON.parse(m)
        }
        ))
      })
    })
  }

  static delete(key: string, value: any) {
    if (!Cache.cache.srem(key, JSON.stringify(value)))
      throw new Error("Error when deleting the data.")
    return true
  }

  static save(key, value) {
    let textvalue = JSON.stringify(value)
    if (!Cache.cache.sadd(key, textvalue))
      throw new Error("Error when saving the data.")
    return true
  }
}