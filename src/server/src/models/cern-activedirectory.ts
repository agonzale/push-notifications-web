const ActiveDirectory = require("activedirectory2").promiseWrapper;
const ldapEscape = require("ldap-escape");

const CERN_AD_config = {
  url: "ldap://xldap.cern.ch",
  baseDN: "dc=cern,dc=ch"
};

export class CERNActiveDirectory {
  static async getTargetsEmails(target: string): Promise<string[]> {
    const filter = ldapEscape.filter`(&(|(mail=${target})(cn=${target}))\
(|(objectClass=group)\
(memberOf=CN=cern-accounts-primary,OU=e-groups,OU=Workgroups,DC=cern,DC=ch)\
(memberOf=CN=cern-accounts-secondary,OU=e-groups,OU=Workgroups,DC=cern,DC=ch)\
(memberOf=CN=cern-accounts-service,OU=e-groups,OU=Workgroups,DC=cern,DC=ch)))`;

    const opts = {
      filter: filter,
      attributes: ["cn", "mail"],
      sizeLimit: 1
    };

    const ad = new ActiveDirectory(CERN_AD_config);

    let result = await ad.find(opts).catch(err => {
      console.error("CERNActiveDirectory.checkExists: ERROR: " + err);
      throw err;
    });

    if (result) {
      if (result.users.length > 0) return [result.users[0].mail];

      if (result.groups.length > 0)
        return (await this.getMembersOfeGroup(result.groups[0].cn)).map(
          user => user.mail
        );
    } else {
      return [];
    }
  }

  static async getTargetCN(target: string): Promise<string> {
    const filter = ldapEscape.filter`(&(|(mail=${target})(cn=${target}))\
(|(objectClass=group)\
(memberOf=CN=cern-accounts-primary,OU=e-groups,OU=Workgroups,DC=cern,DC=ch)\
(memberOf=CN=cern-accounts-secondary,OU=e-groups,OU=Workgroups,DC=cern,DC=ch)\
(memberOf=CN=cern-accounts-service,OU=e-groups,OU=Workgroups,DC=cern,DC=ch)))`;

    const opts = {
      filter: filter,
      attributes: ["cn"],
      sizeLimit: 1
    };

    const ad = new ActiveDirectory(CERN_AD_config);

    let result = await ad.find(opts).catch(err => {
      console.error("CERNActiveDirectory.checkExists: ERROR: " + err);
      throw err;
    });

    if (result) {
      if (result.users.length > 0) return result.users[0].cn;

      if (result.groups.length > 0) return result.groups[0].cn;
    }
  }

  static async getMembersOfeGroup(group: string): Promise<any[]> {
    console.log(`CERNActiveDirectory.getMembersOfeGroup: ${group}`);
    return await this.getMembersOfeGroupRecursive(group);
  }

  static async getMembersOfeGroupRecursive(group: string): Promise<any[]> {
    let users = [];
    const filter = ldapEscape.filter`(&(memberof=CN=${group},OU=e-groups,OU=Workgroups,DC=cern,DC=ch)\
(|(objectClass=group)\
(&(objectClass=user)\
(objectCategory=person)\
(cernAccountType=primary)\
(!(userAccountControl:1.2.840.113556.1.4.803:=2)))))`;

    const opts = {
      filter: filter,
      attributes: ["cn", "mail"]
    };

    const ad = new ActiveDirectory(CERN_AD_config);

    let result = await ad.find(opts).catch(err => {
      console.error("CERNActiveDirectory.checkExists: ERROR: " + err);
      throw err;
    });

    if (result) {
      if (result.users.length > 0) {
        users = [...users, ...result.users];
      }
      if (result.groups.length > 0) {
        for (let group of result.groups) {
          users = [...users, ...(await this.getMembersOfeGroup(group.cn))];
        }
      }
    }

    return users;
  }
}
