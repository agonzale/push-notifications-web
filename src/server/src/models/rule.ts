
import { Notification } from "./notification";

export interface Rule {

  getNotificationChannel(notification: Notification): String;

}