import { User } from "../user";
import { Entity, TableInheritance, ManyToOne, PrimaryGeneratedColumn, Column } from "typeorm";
import { Rule } from "../rule";
import { Notification } from "../notification";
import { Channels } from "./ChannelsList";


@Entity("Rules")
@TableInheritance({ column: { name: "type", type: "varchar" } })
export abstract class AbstractRule implements Rule {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => User, user => user.rules)
  user: User

  @Column({ enum: Channels })
  channel: string;

  constructor(rule) {
    if (rule) {
      this.user = rule.user
      this.channel = rule.channel
    }
  }

  abstract getNotificationChannel(notification: Notification): String;

}