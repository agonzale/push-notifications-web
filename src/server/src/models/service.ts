import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { AuthUtils } from "../utils/authUtils";

@Entity({ name: "Services" })
export class Service {

  @PrimaryGeneratedColumn()
  id: number

  @Column({ unique: true, nullable: false })
  name: string

  @Column({ unique: true, nullable: true })
  apiKey: string

  constructor(name: string) {
    this.name = name
  }

  generateNewApiKey(): string {
    const apiKey = AuthUtils.hash(JSON.stringify({
      name: this.name,
      timeStamp: new Date().getTime()
    }))

    //We have hash the apiKey before store it in the database
    this.apiKey = AuthUtils.hash(apiKey)

    return apiKey;
  }

}