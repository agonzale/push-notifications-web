import { Entity, Column, OneToMany, PrimaryColumn, ManyToMany, JoinTable } from "typeorm";
import { Notification } from "./notification";
import { AbstractRule } from "./rules/abstractRule";
import { Rule } from "./rule";

@Entity({ name: "Users" })
export class User {
  @PrimaryColumn()
  id: number;

  @Column("varchar", { length: 50 })
  username: string;

  @Column()
  email: string

  @ManyToMany(type => Notification, notification => notification.users)
  @JoinTable()
  notifications: Notification[];

  @OneToMany(type => AbstractRule, rule => rule.user)
  rules: Rule[];

  // @Column() enabled: boolean;youyou

  /**
   * 
   * @param user With the following information:
   * @param id Id  of the user
   * @param username  Username of the user
   * @param email Email of the user
   */
  constructor(user: any) {
    if (user) {
      this.id = user.id;
      this.username = user.username;
      this.email = user.email
    }
  }
}
