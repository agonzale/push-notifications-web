import { User } from "../models/user";

export interface AuthService {
  authenticateUser(
    code: string,
    service: string
  ): Promise<{
    randomString: string;
    accessToken: string;
    refreshToken: string;
  }>;

  refreshTokens(
    tokenPayload
  ): Promise<{
    randomString: string;
    accessToken: string;
    refreshToken: string;
  }>;

  getAuthenticatedUser(userId: string): Promise<User>
}
