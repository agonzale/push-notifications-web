import { Command } from "../../command";
import { CernOauth } from "../../../../models/cern-oauth";
import { EntityManager } from "typeorm";
import { Authentication } from "../../../../models/authentication";
import { User } from "../../../../models/user";

export class Login implements Command {

  private cernOauth: CernOauth;

  constructor(private code: string, service: string) {
    this.cernOauth = new CernOauth(service)
  }

  async execute(
    transactionManager: EntityManager
  ): Promise<{
    randomString: string;
    accessToken: string;
    refreshToken: string;
  }> {
    try {
      let user: User = new User(JSON.parse(await this.cernOauth.getUser(this.code)))

      transactionManager.save(user);
      return Authentication.getTokens(user);
    } catch (e) {
      throw new Error("Error when trying to authenticate the user: " + e);
    }
  }
}

