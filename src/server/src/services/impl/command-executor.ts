import { Command } from "./command";
import { getManager, EntityManager } from "typeorm";

export class CommandExecutor {
  execute(command: Command): Promise<any> {
    return getManager().transaction((manager: EntityManager) => {
      return command.execute(manager);
    });
  }
}
