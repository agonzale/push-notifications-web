import { NotificationsService } from "../notifications-service";
import { Notification } from "../../models/notification";
import { SendNotification } from "./notifications/send-notification/send-notification";
import { FindAllNotifications } from "./notifications/find-all-notifications/find-all-notifications";
import { AbstractService } from "./abstract-service";

export class NotificationsServiceImpl extends AbstractService implements NotificationsService {

  sendNotification(notification: Notification): Promise<Notification> {
    return this.commandExecutor.execute(new SendNotification(notification));
  }

  findAllNotifications(userId: number): Promise<Notification[]> {
    return this.commandExecutor.execute(new FindAllNotifications(userId));
  }
}
