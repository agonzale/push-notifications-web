import { Notification } from "../../../../models/notification";
import { NotificationsServiceImpl } from "../../notification-service-impl";
import { TestUtils } from "../../../../utils/testUtils";
import { getManager } from "typeorm";

jest.mock("../../../../models/cache.ts")

describe("Send notification tests", () => {

  beforeAll(async (done) => {
    await TestUtils.createDatabaseConnection()
    done()
  })

  beforeEach(async (done) => {
    await getManager().transaction(async manager => {
      await manager.delete(Notification, {});
    })
    done()
  })


  it("Should return an error when the target does not exist", async () => {
    let notification = new Notification({
      from: "fakeFrom",
      target: "targetFake",
      subject: "subject",
      body: "body"
    })

    await expect(new NotificationsServiceImpl().sendNotification(notification))
      .rejects.toEqual(new Error("Invalid target"))
  })

  it("Should succeed when the target exists", async () => {
    let notification = new Notification({
      from: "fakeFrom",
      target: "test-push",
      subject: "subject",
      body: "body",
    })

    expect(await new NotificationsServiceImpl().sendNotification(notification))
      .toBeInstanceOf(Notification)
  })

})