import { Command } from "../../command";
import { EntityManager, In } from "typeorm";
import { Notification } from "../../../../models/notification";
import { AeroGear } from "../../../../models/aerogear";
import { CERNActiveDirectory } from "../../../../models/cern-activedirectory";
import { User } from "../../../../models/user";
import { Mail } from "../../../../models/mail";
import { Association } from "../../../../models/association";
import { Channels } from "../../../../models/rules/ChannelsList";

export class SendNotification implements Command {
  private aerogear: AeroGear = new AeroGear();

  constructor(private notification: Notification) {
    this.notification = new Notification(notification);
  }

  async execute(transactionalManager: EntityManager) {
    let target: string = this.notification.target;
    console.log("SendNotification.execute: checking target = " + target);

    let mails: string[] = await CERNActiveDirectory.getTargetsEmails(target);
    if (mails.length > 0) {
      console.log(
        `SendNotification.execute: ${target} found, inserting notification.`
      );
      const targetedSystemUsers: User[] = await this.getSystemUsers(transactionalManager, mails)
      this.notification.target = await CERNActiveDirectory.getTargetCN(target);

      await this.saveNotification(targetedSystemUsers, transactionalManager);

      for (let user of targetedSystemUsers) {
        let channels = new Set()
        for (let rule of user.rules) {
          channels.add(rule.getNotificationChannel(this.notification))
        }

        this.sendNotification(channels, user)
      }

      delete this.notification.users
      return this.notification;
    } else {
      console.log(
        `SendNotification.execute: ${target} not found. Returning Invalid target.`
      );
      throw new Error("Invalid target");
    }
  }

  async getSystemUsers(transactionalManager, mails): Promise<User[]> {
    return await transactionalManager.find(
      User,
      {
        where: { email: In(mails) },
        relations: ["notifications", "rules"]
      }
    );
  }


  async saveNotification(
    users,
    transactionalManager: EntityManager
  ) {
    let savedNotification = await transactionalManager.save(this.notification);
    this.notification = savedNotification

    Association.Receive.link(savedNotification, users);

    await transactionalManager.save(users);
  }

  async sendNotification(channels:Set<string>, user: User) {
    if(channels.has(Channels.push)){
      this.sendAssPush(user)
    }
    if(channels.has(Channels.email)){
      this.sendAssEmail(user)
    }
  }

  async sendAssPush(user: User) {
    await this.aerogear.sendNotification(
      this.notification,
      user.username
    );
  }

  async sendAssEmail(user: User) {
    Mail.sendMail(
      "test",
      user.email,
      this.notification.subject,
      this.notification.body
    );
  }
}
