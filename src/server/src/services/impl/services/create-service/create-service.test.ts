import { ServiceFactory } from "../../../services-factory";
import { Service } from "../../../../models/service";
import { getManager, QueryFailedError } from "typeorm";
import { TestUtils } from "../../../../utils/testUtils";

jest.mock("../../../../models/cache.ts")

describe("create service test", () => {

  beforeAll(async (done) => {
    await TestUtils.createDatabaseConnection()
    done()
  })

  beforeEach(async (done) => {
    await getManager().transaction(async manager => {
      await manager.delete(Service, {})
    })
    done()
  })

  afterEach(async (done) => {
    await getManager().transaction(async manager => {
      await manager.delete(Service, {})
    })
    done()
  })

  it("should return a new api key when the service name is provided", async () => {
    const servicesService = ServiceFactory.getServicesService()

    let apiKey: any = await servicesService.createService("service");
    expect(typeof apiKey).toBe("string")

    let service = await servicesService.findServiceByApiKey(apiKey);
    expect(service).toBeInstanceOf(Service);
    expect(service.name).toBe("service")
  })

  it("should return an error when a service with the name already exists", async () => {
    const servicesService = ServiceFactory.getServicesService()
    await servicesService.createService("service");
    await expect(servicesService.createService("service")).rejects.toBeInstanceOf(QueryFailedError)
  })

  it("should return an error when no name is provided", async () => {
    const servicesService = ServiceFactory.getServicesService()
    await expect(servicesService.createService(undefined)).rejects.toBeInstanceOf(QueryFailedError)
  })

})