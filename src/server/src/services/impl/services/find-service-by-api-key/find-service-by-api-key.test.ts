import { TestUtils } from "../../../../utils/testUtils";
import { getManager } from "typeorm";
import { Service } from "../../../../models/service";
import { ServiceFactory } from "../../../services-factory";

jest.mock("../../../../models/cache.ts")

describe("Find service by api key", () => {

  beforeAll(async (done) => {
    await TestUtils.createDatabaseConnection()
    done()
  })

  beforeEach(async (done) => {
    await getManager().transaction(async manager => {
      await manager.delete(Service, {})
    })
    done()
  })

  afterAll(async (done) => {
    await getManager().transaction(async manager => {
      await manager.delete(Service, {})
    })
    done()
  })

  it("should return the service when the api key is correct", async (done) => {
    const servicesService = ServiceFactory.getServicesService()
    const apiKey = await servicesService.createService("service-find");

    const service = await servicesService.findServiceByApiKey(apiKey)
    expect(service).toBeInstanceOf(Service);
    expect(service.name).toBe("service-find")
    done()
  })

  it("should return no service when the api key is not correct", async () => {
    await getManager().transaction(async manager => {
      await manager.delete(Service, {})
    })
    const servicesService = ServiceFactory.getServicesService()
    await servicesService.createService("service-find");

    const service = await servicesService.findServiceByApiKey("someRandomApiKey")
    expect(service).toBeUndefined()
  })
})