import { Command } from "../../command";
import { EntityManager } from "typeorm";
import { Service } from "../../../../models/service";
import { AuthUtils } from '../../../../utils/authUtils';


export class FindServiceByApiKey implements Command {

  constructor(private apiKey: string) { }

  execute(transactionManager: EntityManager): Promise<Service> {
    const hashedApiKey = AuthUtils.hash(this.apiKey)
    return transactionManager.findOne(Service, { where: { apiKey: hashedApiKey } })
  }

}