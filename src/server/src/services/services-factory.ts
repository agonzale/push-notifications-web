import { NotificationsService } from "./notifications-service";
import { NotificationsServiceImpl } from "./impl/notification-service-impl";
import { AuthServiceImpl } from "./impl/auth-service-impl";
import { AuthService } from "./auth-service";
import { ServicesService } from "./services-service";
import { ServicesServiceImpl } from "./impl/services-service-impl";
import { RulesService } from "./rules-service";
import { RulesServiceImpl } from "./impl/rules-service-impl";

export class ServiceFactory {
  static getNotificationsService(): NotificationsService {
    return new NotificationsServiceImpl();
  }

  static getAuthenticationService(): AuthService {
    return new AuthServiceImpl();
  }

  static getServicesService(): ServicesService {
    return new ServicesServiceImpl();
  }

  static getRulesService(): RulesService {
    return new RulesServiceImpl();
  }
}
