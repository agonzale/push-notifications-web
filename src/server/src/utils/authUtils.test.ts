import { AuthUtils } from "./authUtils";

describe("Auth utils tests", () => {
  describe("Hash fingerprint test", () => {
    it("Should return ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad when the input is 'abc'", () => {
      expect(AuthUtils.hash("abc")).toBe("ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad")
    })

    it("Should return e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855 when the input is ''", () => {
      expect(AuthUtils.hash("")).toBe("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855")
    })

    it("Should return 248d6a61d20638b8e5c026930c3e6039a33ce45964ff2167f6ecedd419db06c1 when the input is 'abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq'", () => {
      expect(AuthUtils.hash("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq")).toBe("248d6a61d20638b8e5c026930c3e6039a33ce45964ff2167f6ecedd419db06c1")
    })

    it("Should return cf5b16a778af8380036ce59e7b0492370b249b11e8f07a51afac45037afee9d1 when the input is 'abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu'", () => {
      expect(AuthUtils.hash("abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu")).toBe("cf5b16a778af8380036ce59e7b0492370b249b11e8f07a51afac45037afee9d1")
    })
  })
})