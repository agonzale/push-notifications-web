import * as crypto from "crypto";

export class AuthUtils {
  static hash(fingerPrint: string): string {
    return crypto
      .createHash(process.env.JWT_FINGERPRINT_HASHING_ALGORITHM || "sha256")
      .update(fingerPrint)
      .digest("hex");
  }
}
