import { createConnection } from "typeorm";
import { ConfigureENVs } from "../config/configuration";

export class TestUtils {

  static createDatabaseConnection = () => {
    ConfigureENVs.config(process.env.NODE_ENV)
    return createConnection()
  }
}  